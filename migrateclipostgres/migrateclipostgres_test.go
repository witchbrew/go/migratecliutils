package migrateclipostgres

import (
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestAddPersistentFlags(t *testing.T) {
	c := &cobra.Command{
		Use: "test",
	}
	AddPersistentFlags(c)
	c.SetArgs([]string{})
	err := c.Execute()
	require.Nil(t, err)
}
