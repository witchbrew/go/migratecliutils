package migrateclipostgres

import (
	"database/sql"
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	flag "github.com/spf13/pflag"
	"gitlab.com/witchbrew/go/migrate/driver"
	"gitlab.com/witchbrew/go/migrate/postgres"
)

var pgFlags = &struct {
	user     string
	password string
	host     string
	database string
	sslMode  string
}{}

func addFlags(f *flag.FlagSet) {
	f.StringVarP(&pgFlags.user, "user", "u", "", "")
	f.StringVarP(&pgFlags.password, "password", "p", "", "")
	f.StringVarP(&pgFlags.host, "host", "o", "", "")
	f.StringVarP(&pgFlags.database, "database", "d", "", "")
	f.StringVar(&pgFlags.sslMode, "sslmode", "disable", "")
}

func AddPersistentFlags(command *cobra.Command) {
	addFlags(command.PersistentFlags())
}

func AddFlags(command *cobra.Command) {
	addFlags(command.Flags())
}

func Driver(versionTableName string) (driver.Driver, error) {
	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=%s",
		pgFlags.user,
		pgFlags.password,
		pgFlags.host,
		pgFlags.database,
		pgFlags.sslMode,
	)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open DB")
	}
	drv := postgres.Driver(db, versionTableName)
	return drv, nil
}
