package migratecliutils

import (
	"context"
	"errors"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/migrate"
	"gitlab.com/witchbrew/go/migrate/migration"
	"gitlab.com/witchbrew/go/migrate/mockdriver"
	"gitlab.com/witchbrew/go/migrate/testdriver"
	"gitlab.com/witchbrew/go/migrate/version"
	"io/ioutil"
	"testing"
)

func TestSetupLogger(t *testing.T) {
	drv := testdriver.New()
	SetupMigrate(drv, migrate.Migrations{&migration.Migration{}})
	SetupLogger()
}

func TestSimpleEmptyMigration(t *testing.T) {
	c := &cobra.Command{
		Use: "test",
	}
	AddAllCommands(c)
	drv := testdriver.New()
	SetupMigrate(drv, migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return nil
		},
		Down: func(ctx context.Context) error {
			return nil
		},
	}})
	c.SetArgs([]string{"up"})
	err := c.Execute()
	require.Nil(t, err)
	v, err := drv.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 1, Dirty: false}, v)
	c.SetArgs([]string{"down"})
	err = c.Execute()
	require.Nil(t, err)
	v, err = drv.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 0, Dirty: false}, v)
}

func TestUpErr(t *testing.T) {
	c := &cobra.Command{
		Use: "test",
	}
	c.SetOut(ioutil.Discard)
	AddAllCommands(c)
	drv := testdriver.New()
	SetupMigrate(drv, migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return errors.New("test err")
		},
	}})
	c.SetArgs([]string{"up"})
	err := c.Execute()
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "failed to run up")
}

func TestDownErr(t *testing.T) {
	c := &cobra.Command{
		Use: "test",
	}
	c.SetOut(ioutil.Discard)
	AddAllCommands(c)
	drv := testdriver.New()
	SetupMigrate(drv, migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return nil
		},
		Down: func(ctx context.Context) error {
			return errors.New("test err")
		},
	}})
	c.SetArgs([]string{"up"})
	err := c.Execute()
	require.Nil(t, err)
	c.SetArgs([]string{"down"})
	err = c.Execute()
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "failed to run down")
}

func TestBackOff(t *testing.T) {
	c := &cobra.Command{
		Use: "test",
	}
	c.SetOut(ioutil.Discard)
	AddAllCommands(c)
	drv := mockdriver.New()
	drv.Mock.On("Check").Return(false, nil)
	SetupMigrate(drv, migrate.Migrations{})
	c.SetArgs([]string{"--back-off", "10ms", "--retries", "3", "up"})
	err := c.Execute()
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "failed waiting migrate: timeout waiting for driver")
}
