module gitlab.com/witchbrew/go/migratecliutils

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.19.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.6.1
	gitlab.com/witchbrew/go/migrate v0.0.0-20201003212111-9a9b0b6e96c2
)
