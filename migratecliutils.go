package migratecliutils

import (
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/witchbrew/go/migrate"
	"gitlab.com/witchbrew/go/migrate/driver"
	"os"
	"time"
)

var m *migrate.Migrate

type backOffFlagsStruct struct {
	backOff time.Duration
	retries uint64
}

func (bof *backOffFlagsStruct) isSet() bool {
	return bof.backOff > 0 && bof.retries > 0
}

var backOffFlags = backOffFlagsStruct{}

func addBackoffFlags(command *cobra.Command) {
	flagSet := command.Flags()
	flagSet.DurationVar(&backOffFlags.backOff, "back-off", 0, "")
	flagSet.Uint64Var(&backOffFlags.retries, "retries", 0, "")
}
func init() {
	addBackoffFlags(UpCmd)
	addBackoffFlags(DownCmd)
}

func SetupMigrate(drv driver.Driver, migrations migrate.Migrations) {
	m = migrate.New(drv, migrations)
}

func SetupLogger() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	m.Logger = log.Logger
}

var UpCmd = &cobra.Command{
	Use: "up",
	RunE: func(cmd *cobra.Command, args []string) error {
		if backOffFlags.isSet() {
			err := m.WaitReady(backOffFlags.backOff, backOffFlags.retries)
			if err != nil {
				return errors.Wrap(err, "failed waiting migrate")
			}
		}
		err := m.Init()
		if err != nil {
			return errors.Wrap(err, "failed to init migrate")
		}
		err = m.Up()
		if err != nil {
			return errors.Wrap(err, "failed to run up")
		}
		return nil
	},
}

var DownCmd = &cobra.Command{
	Use: "down",
	RunE: func(cmd *cobra.Command, args []string) error {
		if backOffFlags.isSet() {
			err := m.WaitReady(backOffFlags.backOff, backOffFlags.retries)
			if err != nil {
				return errors.Wrap(err, "failed waiting migrate")
			}
		}
		err := m.Init()
		if err != nil {
			return errors.Wrap(err, "failed to init migrate")
		}
		err = m.Down()
		if err != nil {
			return errors.Wrap(err, "failed to run down")
		}
		return nil
	},
}

func AddAllCommands(parentCommand *cobra.Command) {
	parentCommand.AddCommand(UpCmd)
	parentCommand.AddCommand(DownCmd)
}
